<?php

namespace Drupal\view_mode_preview;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Helper class.
 *
 * @TODO: this class should be refactored.
 */
class PreviewBuilder {

  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Information about the entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $viewModeEntityDefinition;

  /**
   * All entity types.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface[]
   */
  protected $entityTypeDefinitions;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Creates a PreviewBuilder service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info, ConfigFactoryInterface $config_factory, RequestStack $request_stack) {
    $this->entityTypeManager = $entity_type_manager;
    $this->viewModeEntityDefinition = $this->entityTypeManager->getDefinition('entity_view_mode');
    $this->entityTypeDefinitions = $this->entityTypeManager->getDefinitions();
    $this->messenger = $messenger;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
  }

  /**
   * Build preview.
   *
   * @param string $entity_type
   *   Entity type.
   *
   * @return array
   *   Renderable array of preview content.
   */
  public function build($entity_type) {
    $build = [];
    $view_builder = $this->entityTypeManager
      ->getViewBuilder($entity_type);
    $parameters = $this->getValidatedParameters($entity_type);
    $view_mode = $parameters['view_mode'] ?? '';

    unset($parameters['view_mode']);
    foreach ($parameters as $index => $parameter) {
      try {
        $entity = $this->entityTypeManager
          ->getStorage($entity_type)
          ->load($parameter);
      }
      catch (Exception $e) {
        $this->messenger->addError($this->t('There was a problem building preview: @message', ['@message' => $e->getMessage()]));
      }
      if (isset($entity)) {
        $build[] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['view-mode-' . $view_mode . '-' . $index],
          ],
          'label' => [
            '#type' => 'html_tag',
            '#tag' => 'h2',
            '#value' => 'Bundle: ' . $index,
          ],
          'markup' => $view_builder->view($entity, $view_mode),
        ];
      }
    }
    return $build;
  }

  /**
   * Get validated query parameters.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $view_mode
   *   View mode name.
   *
   * @return array
   *   Array of parameters.
   */
  public function getValidatedParameters($entity_type, $view_mode = NULL) {
    // Get query parameters.
    $parameters = $this->requestStack->getCurrentRequest()->query->all();
    if (!$view_mode) {
      $view_mode = $parameters['view_mode'] ?? 'default';
    }
    // Filter query parameters.
    $allowed = array_merge(['view_mode'], $this->getAvailableBundles($entity_type, $view_mode));
    $parameters = array_intersect_key($parameters, array_flip($allowed));

    return $parameters;
  }

  /**
   * Get all bundles which are using view_mode.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $view_mode
   *   View mode name.
   *
   * @return array
   *   Array of bundle names.
   */
  public function getAvailableBundles($entity_type, $view_mode) {
    $bundle_ids = [];

    if ($view_mode == 'default') {
      $bundle_ids = $this->entityTypeBundleInfo
        ->getBundleInfo($entity_type);
      $bundle_ids = array_keys($bundle_ids);
    }
    else {
      $config_ids = $this->configFactory
        ->listAll('core.entity_view_display.' . $entity_type . '.');
      foreach ($config_ids as $config_id) {
        list(, , , $bundle_id, $config_view_mode) = explode('.', $config_id);
        if ($config_view_mode == $view_mode && $config_view_mode != 'default') {
          $bundle_ids[] = $bundle_id;
        }
      }
    }

    return $bundle_ids;
  }

  /**
   * Get a list of entity types that have available view modes.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   Array of entity types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAvailableEntityTypes() {
    $storage = $this->entityTypeManager->getStorage('entity_view_mode');
    $query = $storage->getQuery();
    $entity_ids = $query->execute();
    $view_mode_entities = $storage->loadMultipleOverrideFree($entity_ids);
    uasort($view_mode_entities, [
      $this->viewModeEntityDefinition->getClass(),
      'sort',
    ]);
    $entities = [];
    foreach ($view_mode_entities as $entity) {
      $entities[$entity->getTargetType()][] = $entity;
    }

    foreach ($entities as $entity_type => $entity) {
      // Filter entities.
      if (!isset($this->entityTypeDefinitions[$entity_type]) || !$this->isValidEntity($entity_type)) {
        unset($entities[$entity_type]);
      }
    }

    return array_intersect_key($this->entityTypeDefinitions, $entities);
  }

  /**
   * Filters entities based on their view builder handlers.
   *
   * @param string $entity_type
   *   The entity type of the entity that needs to be validated.
   *
   * @return bool
   *   TRUE if the entity has the correct view builder handler, FALSE if the
   *   entity doesn't have the correct view builder handler.
   */
  protected function isValidEntity($entity_type) {
    if (!isset($this->entityTypeDefinitions[$entity_type])) {
      return FALSE;
    }

    return $this->entityTypeDefinitions[$entity_type]->get('field_ui_base_route') && $this->entityTypeDefinitions[$entity_type]->hasViewBuilderClass();
  }

  /**
   * Get last entity of bundle.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Entity bundle.
   *
   * @return int|null
   *   Entity id or null.
   */
  public function getLastEntity($entity_type, $bundle = NULL) {
    // Get entity type definition.
    try {
      $entity_type_definition = $this->entityTypeManager
        ->getDefinition($entity_type);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException($e->getMessage());
    }

    // Get entity storage for given entity type.
    try {
      $entity_storage = $this->entityTypeManager->getStorage($entity_type);
    }
    catch (Exception $e) {
      throw new NotFoundHttpException($e->getMessage());
    }
    // Build query.
    $query = $entity_storage->getQuery();
    $query->condition('status', 1);
    if (isset($bundle) && $entity_type_definition->hasKey('bundle')) {
      $query->condition($entity_type_definition->getKey('bundle'), $bundle);
    }
    $query->sort($entity_type_definition->getKey('id'), 'DESC');
    $query->range(0, 1);
    $entity_ids = $query->execute();
    $entity_ids = array_values($entity_ids);
    return array_shift($entity_ids);
  }

}
