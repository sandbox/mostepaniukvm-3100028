<?php

namespace Drupal\view_mode_preview\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\view_mode_preview\PreviewBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu links for the Preview view modes.
 */
class PreviewMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Preview helper service instance.
   *
   * @var \Drupal\view_mode_preview\PreviewBuilder
   */
  protected $previewBuilder;

  /**
   * Creates a PreviewMenuLink instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\view_mode_preview\PreviewBuilder $preview_builder
   *   Preview helper service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PreviewBuilder $preview_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->previewBuilder = $preview_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('view_mode_preview.preview_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    // Get available entity types.
    $entity_types = $this->previewBuilder->getAvailableEntityTypes();
    // Build menu link for each entity type.
    foreach ($entity_types as $id => $entity_type) {
      $links[$id] = [
          'title' => $entity_type->getLabel(),
          'route_name' => 'view_mode_preview.entity_preview',
          'route_parameters' => ['entity_type' => $id],
          'parent' => 'view_mode_preview.admin',
        ] + $base_plugin_definition;
    }

    return $links;
  }

}
