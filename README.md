CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

View mode preview module is a helper module that allows content managers, 
testers and developers to preview any content entities with any available view 
modes. It provides menu links per entity type in Content -> Preview admin menu 
section. You can select entities you want to preview and available view mode for
selected entity type. You can easily get a link and send it to any team member.
You can use filter form or you can set entity id as query parameter. Also you
can set last entities as default values.


REQUIREMENTS
------------

Module doesn't have any special requirements because has deal only with core's
functionality.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8 for further
information.


CONFIGURATION
-------------

* Just enable module and start using.


MAINTAINERS
-----------

Current maintainers:
 * Volodymyr Mostepaniuk (mostepaniukvm) -
https://www.drupal.org/u/mostepaniukvm
